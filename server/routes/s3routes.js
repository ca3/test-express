const multer = require("multer")
var AWS = require('aws-sdk')
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });

class AWSFactory {
    constructor(props) {
        this.app = props.app


        let s3bucket = new AWS.S3({
            accessKeyId: props.publickey,
            secretAccessKey: props.privatekey,
            region: props.region
        });

        this.app.get('/', function(req, res) {
            return res.status(200).json({platform: props.platform})
        })

        this.app.post('/upload', upload.single("file"), function(req, res) {
            const file = req.file;
            const s3FileURL = props.uploadurl;

            var params = {
                Bucket: props.bucketname,
                Key: file.originalname,
                Body: file.buffer,
                ContentType: file.mimetype,
                ACL: "public-read"
            };

            s3bucket.upload(params, function(err, data) {
                if (err) {
                    res.status(500).json({ error: true, Message: err });
                } else {
                    res.send({ data });
                    var newFileUploaded = {
                        description: req.body.description,
                        fileLink: s3FileURL + file.originalname,
                        s3_key: params.Key
                    };
                    console.log(newFileUploaded.fileLink)
                }
            });
        })
    }
}

module.exports = {AWSFactory};



