require('dotenv').config();
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser')
const PORT = process.env.PORT || 3000
const {UploadsFactory} = require('./routes');

const app = express()

app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

let platform = process.env.PLATFORM;
if(platform === "Azure" || platform === "S3" || platform === "LOCAL") {
    let upload = {}
    let uploadProps = {
        platform,
        publickey: process.env.PUBLIC_KEY,
        privatekey: process.env.PRIVATE_KEY,
        uploadurl: process.env.UPLOAD_URL,
        region: process.env.REGION,
        bucketname: process.env.BUCKET_NAME,
        accountname: process.env.ACCOUNT_NAME,
        app
    }

    upload.upload = new UploadsFactory(platform, uploadProps)

} else {
    app.get('/', (req, res) => {
        return res.status(400).json({error: "Must supply storage location"})
    })
}



app.listen(PORT, function(){
    console.log(`server is starting on ${PORT}`)
})



