const {
    Aborter,
    BlobURL,
    BlockBlobURL,
    ContainerURL,
    ServiceURL,
    StorageURL,
    SharedKeyCredential,
    uploadStreamToBlockBlob
} = require('@azure/storage-blob');
const multer = require("multer")
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });
const getStream = require('into-stream');
const containerName = 'images';
const ONE_MEGABYTE = 1024 * 1024;
const uploadOptions = { bufferSize: 4 * ONE_MEGABYTE, maxBuffers: 20 };
const ONE_MINUTE = 60 * 1000;
const aborter = Aborter.timeout(30 * ONE_MINUTE);




const getBlobName = originalName => {
    // Use a random number to generate a unique file name,
    // removing "0." from the start of the string.
    const identifier = Math.random().toString().replace(/0\./, '');
    return `${identifier}-${originalName}`;
};

class AzureFactory {
    constructor(props) {
        this.app = props.app

        const sharedKeyCredential = new SharedKeyCredential(
            props.accountname,
            props.privatekey);
        const pipeline = StorageURL.newPipeline(sharedKeyCredential);
        const serviceURL = new ServiceURL(
            `https://${props.accountname}.blob.core.windows.net`,
            pipeline
        );

        this.app.get('/', function(req, res) {
            return res.status(200).json({platform: props.platform})
        })
        this.app.post('/upload', upload.single("file"), async function(req, res) {

            const blobName = getBlobName(req.file.originalname);
            const stream = getStream(req.file.buffer);
            const containerURL = ContainerURL.fromServiceURL(serviceURL, containerName);
            const blobURL = BlobURL.fromContainerURL(containerURL, blobName);
            const blockBlobURL = BlockBlobURL.fromBlobURL(blobURL);

            try {

                await uploadStreamToBlockBlob(aborter, stream,
                    blockBlobURL, uploadOptions.bufferSize, uploadOptions.maxBuffers);

                res.render('success', { message: 'File uploaded to Azure Blob storage.' });

            } catch (err) {

                res.render('error', { message: 'Something went wrong.' });

            }
        })
    }
}

module.exports = {AzureFactory};



