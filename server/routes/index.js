const {LocalFactory} = require("./localRoutes")
const {AWSFactory} = require("./s3routes")
const {AzureFactory} = require("./azureRoutes")

class UploadsFactory {
    constructor(type, props) {
        if(type === "LOCAL") {
            return new LocalFactory(props)
        }
        if(type === "S3") {
            return new AWSFactory(props)
        }
        if(type === "Azure") {
            return new AzureFactory(props)
        }
    }
}
module.exports = {UploadsFactory};
