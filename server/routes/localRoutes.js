class LocalFactory {
    constructor(props) {
        this.app = props.app

        this.app.get('/', function(req, res) {
            return res.status(200).json({platform: props.platform})
        })
    }
}

module.exports = {LocalFactory};



